## back-end

Este projeto foi desenvolvido utilizando as tecnologias:

- SpringBoot
- Swagger
- Lombok

Demostraçåo online:
https://daraujo-backend-celk.herokuapp.com/v1/api

Intalação:

```sh
mvn clean install
```
Testes:
```sh
mvn test
```

Excução:
```sh
mvn spring-boot:run
```

[SpringBoot](https://spring.io/projects/spring-boot) Spring Boot makes it easy to create stand-alone, production-grade Spring based Applications that you can "just run".
[Swagger](https://swagger.io/) Simplify API development for users, teams, and enterprises with the Swagger open source and professional toolset. Find out how Swagger can help you design and document your APIs at scale.
[Lombok](https://projectlombok.org/) Project Lombok is a java library that automatically plugs into your editor and build tools, spicing up your java.
