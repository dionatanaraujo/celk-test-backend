package br.com.celk.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CELKApplication {

	public static void main(String[] args) {
		SpringApplication.run(CELKApplication.class, args);
	}


	// 1. fazer um app
	// 2. add ribbon
	// 3. swagger
	// 4. flyway


}
