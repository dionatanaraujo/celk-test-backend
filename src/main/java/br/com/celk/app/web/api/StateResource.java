package br.com.celk.app.web.api;


import br.com.celk.app.model.State;
import br.com.celk.app.repository.StateRepository;
import br.com.celk.app.web.exeptions.BadRequestException;
import br.com.celk.app.web.util.PaginationUtil;
import br.com.celk.app.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = {"https://daraujo-frontend-celk.herokuapp.com", "http://localhost:3000"})

@RestController
@RequestMapping("/api/v1/states")
public class StateResource {

    private final Logger log = LoggerFactory.getLogger(StateResource.class);

    private final StateRepository repository;

    public StateResource(StateRepository repository) {
        this.repository = repository;
    }

    /**
     * {@code POST  /states} : Create a new state.
     *
     * @param state the state to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new state, or with status {@code 400 (Bad Request)} if the state has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping
    public ResponseEntity<State> create(@Valid @RequestBody State state) throws URISyntaxException, BadRequestException {
        log.debug("REST request to save State : {}", state);
        if (state.getId() != null) {
            throw new BadRequestException("Invalid id", "State", "idnull");
        }
        state.setCreatedAt(LocalDateTime.now());
        State result = repository.save(state);
        return ResponseEntity.created(new URI("/api/v1/states/" + result.getId()))
                .body(result);
    }

    /**
     * {@code PUT  /states} : Updates an existing state.
     *
     * @param state the state to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated state,
     * or with status {@code 400 (Bad Request)} if the state is not valid,
     * or with status {@code 500 (Internal Server Error)} if the state couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping
    public ResponseEntity<State> update(@Valid @RequestBody State state) throws BadRequestException {
        log.debug("REST request to update State : {}", state);
        if (state.getId() == null) {
            throw new BadRequestException("Invalid id:", "state", "idnull");
        }
        State result = repository.save(state);
        return ResponseEntity.ok()
                .body(result);
    }

    /**
     * {@code GET  /states} : get all the states.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of states in body.
     */
    @GetMapping
    public ResponseEntity<List<State>> getAll(Pageable pageable) {
        log.debug("REST request to get a page of States");
        Page<State> page = repository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /states/:id} : get the "id" state.
     *
     * @param id the id of the state to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the state, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<State> getOne(@PathVariable Long id) {
        log.debug("REST request to get State : {}", id);
        Optional<State> state = repository.findById(id);
        return ResponseUtil.wrapOrNotFound(state);
    }

    /**
     * {@code DELETE  /states/:id} : delete the "id" state.
     *
     * @param id the id of the state to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.debug("REST request to delete State : {}", id);
        repository.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
