package br.com.celk.app.web.api;

import br.com.celk.app.CELKApplication;
import br.com.celk.app.model.State;
import br.com.celk.app.repository.StateRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link StateResource} REST controller.
 */
@SpringBootTest(classes = CELKApplication.class)

@AutoConfigureMockMvc
public class StateResourceTest {

    private static final String DEFAULT_NAME = "Santa Catarina";
    private static final String DEFAULT_INITIALS = "SC";
    private static final LocalDateTime DEFAULT_CREATED_DATE = LocalDateTime.now();

    @Autowired
    private StateRepository repository;

    @Autowired
    private MockMvc restStateMockMvc;

    private State state;

    @BeforeEach
    public void initTest() {
        state = createEntity();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static State createEntity() {
        return State.builder()
                .acronym(DEFAULT_INITIALS)
                .name(DEFAULT_NAME)
                .createdAt(DEFAULT_CREATED_DATE)
                .build();
    }

    @Test
    @Transactional
    public void createState() throws Exception {
        int databaseSizeBeforeCreate = repository.findAll().size();

        // Create the State
        restStateMockMvc.perform(post("/api/v1/states")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(state)))
                .andExpect(status().isCreated());

        // Validate the State in the database
        List<State> stateList = repository.findAll();
        assertThat(stateList).hasSize(databaseSizeBeforeCreate + 1);
        State testState = stateList.get(stateList.size() - 1);
        assertThat(testState.getAcronym()).isEqualTo(DEFAULT_INITIALS);
        assertThat(testState.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testState.getCreatedAt()).isNotNull();
    }


    @Test
    @Transactional
    public void checkInitialsIsRequired() throws Exception {
        int databaseSizeBeforeTest = repository.findAll().size();
        // set the field null
        state.setAcronym(null);

        // Create the State, which fails.

        restStateMockMvc.perform(post("/api/v1/states")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(state)))
                .andExpect(status().isBadRequest());

        List<State> stateList = repository.findAll();
        assertThat(stateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = repository.findAll().size();
        // set the field null
        state.setName(null);

        // Create the State, which fails.

        restStateMockMvc.perform(post("/api/v1/states")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(state)))
                .andExpect(status().isBadRequest());

        List<State> stateList = repository.findAll();
        assertThat(stateList).hasSize(databaseSizeBeforeTest);
    }


    @Test
    @Transactional
    public void getAllStates() throws Exception {
        // Initialize the database
        repository.saveAndFlush(state);

        // Get all the stateList
        restStateMockMvc.perform(get("/api/v1/states?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(state.getId().intValue())))
                .andExpect(jsonPath("$.[*].acronym").value(hasItem(DEFAULT_INITIALS)))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
                .andExpect(jsonPath("$.[*].createdAt").exists());
    }

    @Test
    @Transactional
    public void getState() throws Exception {
        // Initialize the database
        repository.saveAndFlush(state);

        // Get the state
        restStateMockMvc.perform(get("/api/v1/states/{id}", state.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id").value(state.getId().intValue()))
                .andExpect(jsonPath("$.acronym").value(DEFAULT_INITIALS))
                .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
                .andExpect(jsonPath("$.createdAt").exists());
    }

    @Test
    @Transactional
    public void getNonExistingState() throws Exception {
        // Get the state
        restStateMockMvc.perform(get("/api/v1/states/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void deleteState() throws Exception {
        // Initialize the database
        repository.saveAndFlush(state);

        int databaseSizeBeforeDelete = repository.findAll().size();

        // Delete the state
        restStateMockMvc.perform(delete("/api/v1/states/{id}", state.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<State> stateList = repository.findAll();
        assertThat(stateList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
